var should = require('should');
var encryption = require('../../lib/encryption');
describe('encryption', function () {
    describe('uid', function () {
        it('10 million default identifiers', function () {
            this.timeout(10000);
            var i = 10000000;
            while (i--) {
                encryption.uid();
            }
        });
        it('1 million identifiers length 1024', function () {
            this.timeout(10000);
            var i = 1000000;
            var len = 1024;
            while (i--) {
                encryption.uid(len);
            }
        });
    });
});
